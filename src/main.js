// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import store from './store'
import App from './App'
import router from './router'
import './css/main.scss'

// Apollo
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
import {GC_AUTH_TOKEN, GC_USER_ID, SELECTED_LIST_ID} from './graphql/graphql'
import {ApolloLink, split} from 'apollo-link'
import {WebSocketLink} from 'apollo-link-ws'
import {getMainDefinition} from 'apollo-utilities'

Vue.config.productionTip = false

Vue.config.productionTip = false

// 3
const httpLink = new HttpLink({
  // You should use an absolute URL here
  uri: 'https://api.graph.cool/simple/v1/cji55qkux1c6x0167vo5hb64f'
})

// Create the subscription websocket link
const wsLink = new WebSocketLink({
  uri: 'wss://subscriptions.graph.cool/v1/cji55qkux1c6x0167vo5hb64f',
  options: {
    reconnect: true,
    connectionParams: () => ({
      authToken: localStorage.getItem(GC_AUTH_TOKEN)
    })
  }
})

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  const token = localStorage.getItem(GC_AUTH_TOKEN)
  operation.setContext({
    headers: {
      authorization: token ? `Bearer ${token}` : null
    }
  })

  return forward(operation)
})

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' &&
      operation === 'subscription'
  },
  wsLink,
  authMiddleware.concat(httpLink)
)

// 4
const apolloClient = new ApolloClient({
  link: link,
  cache: new InMemoryCache(),
  connectToDevTools: true
})

// 5
Vue.use(VueApollo)

// 6
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $loadingKey: 'loading'
  }
})

let userId = localStorage.getItem(GC_USER_ID)
let selectedList = localStorage.getItem(SELECTED_LIST_ID)
let isAddMember = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store, // inject store to all children
  template: '<App/>',
  components: { App },
  provide: apolloProvider.provide(),
  data: {
    userId,
    isAddMember,
    selectedList
  }
})
