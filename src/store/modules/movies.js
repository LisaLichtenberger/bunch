// import server from '../../api/server'

// initial state
const state = {
  all: [],

  user: {
    id: 1,
    name: 'Lisa',
    email: 'lisa.lichtenberger9@gmail.com'
  },

  movieLists: [
    {
      name: 'WG',
      movieList: [
        {
          movieId: 'tt0499549', // imdbID --> not required if custom movie
          title: 'Avatar'
        },
        {
          movieId: 'tt0076759', // not required if custom movie
          title: 'Star Wars: Episode IV - A New Hope'
        },
        {
          movieId: 'tt1837492', // not required if custom movie
          title: '13 Reasons Why - Season 2'
        },
        {
          movieId: 'tt3743822', // not required if custom movie
          title: 'Fear the Walking Dead'
        },
        {
          movieId: 'tt2372162', // not required if custom movie
          title: 'Orange is the new Black'
        },
        {
          movieId: 'tt0417299', // not required if custom movie
          title: 'Avatar - The Last Airbender'
        },
        {
          movieId: '', // not required if custom movie
          title: 'Custom film with no imdbID'
        }
      ]
    },
    {
      name: 'Home',
      movieList: [
        {
          movieId: 'tt2372162', // not required if custom movie
          title: 'Orange is the new Black'
        },
        {
          movieId: 'tt0417299', // not required if custom movie
          title: 'Avatar - The Last Airbender'
        },
        {
          movieId: '', // not required if custom movie
          title: 'Custom film with no imdbID'
        }
      ]
    },
    {
      name: 'Besties',
      movieList: [
        {
          movieId: 'tt0092099', // not required if custom movie
          title: 'Top Gun'
        }
      ]
    },
    {
      name: 'Besties',
      movieList: []
    }
  ]
}

// getters
const getters = {
  moviesOfList: (state, getters, rootState) => {
    return state.movieLists[0]
  }
}

// actions
const actions = {
  getMoviesOfFirstList ({ commit }) {
    /** server.getMovies(movies => {
      commit('setMovies', movies)
    }) **/
  },
  getListOfMovies ({ commit }) {

  },
  addMovieToList ({ state, commit }, movie) {
    commit('addMovieToList', { movie: movie })
  }
}

// mutations
const mutations = {
  setMovieLists (state, movieLists) {
    state.movieLists = movieLists
  },
  addMovieToList (state, { movie }) {
    state.movieList[0].push(movie)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
