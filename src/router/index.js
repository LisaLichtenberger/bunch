import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import ItemsContainer from '@/components/ItemsContainer'
import SearchBar from '@/components/SearchBar'
import ListsContainer from '@/components/ListsContainer'
import ListDetail from '@/components/ListDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Default',
      component: ItemsContainer
    },
    {
      path: '/home',
      name: 'Home',
      component: ItemsContainer
    },
    {
      path: '/list-detail',
      name: 'ListDetail',
      component: ListDetail
    },
    {
      path: '/lists',
      name: 'ListsListsContainer',
      component: ListsContainer
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Login
    },
    {
      path: '/searchbar',
      name: 'SearchBar',
      component: SearchBar
    }
  ]
})
