import gql from 'graphql-tag'

export const GC_USER_ID = 'graphcool-user-id'
export const GC_AUTH_TOKEN = 'graphcool-auth-token'
export const SELECTED_LIST_ID = 'selected_list_id'

export const CREATE_USER_MUTATION = gql`
  mutation CreateUserMutation($email: String!, $password: String!) {
    signupUser(
      email: $email,
      password: $password,
    ) {
      id
    }
    
    authenticateUser(
      email: $email,
      password: $password
      ) {
      token
      id
    } 
  }
`

export const SIGNIN_USER_MUTATION = gql`
  mutation SigninUserMutation($email: String!, $password: String!) {
    authenticateUser(
      email: $email,
      password: $password
      ) {
      token
      id
    }
  }
`

export const CREATE_MOVIELIST_MUTATION = gql`
  mutation CreateMovieList($name: String!, $userId: ID!) {
    createMovieList(name: $name, usersIds: [$userId]) {
      id
      name
      users {
        email
      }
    }
  }
`

export const ADD_USER_TO_MOVIELIST_MUTATION = gql`
  mutation AddUserToMovieList($userId: ID!, $movieListId: ID!) {
    addToUserLists(usersUserId: $userId movieListsMovieListId: $movieListId) {
      usersUser {
        email
      }
    }

    updateMovieList(id: $movieListId update: "Update") {
      id
    }
  }
`

export const ADD_MOVIE_TO_LIST_MUTATION = gql`
  mutation AddMovieToList($title: String!, $userId: ID!, $movieListId: ID!, $movieId: String) {
    createMovie(title: $title, userId: $userId, movieListId: $movieListId, movieId: $movieId) {
      id
      title
      movieId
    }
    
    updateMovieList(id: $movieListId update: "Update") {
      id
    }
  }
`

export const DELETE_MOVIE = gql`
  mutation DeleteMovie($id: ID!) {
    deleteMovie(id: $id){
      id
    }
  }
`

export const DELETE_MOVIE_LIST = gql`
  mutation DeleteMovieList($id: ID!) {
    deleteMovieList(id: $id){
      id
    }
  }
`

export const SUB_MOVIE_LIST = gql`
  subscription changedMovieList($userId: ID!) {
    MovieList(
      filter: {
        mutation_in: [CREATED, UPDATED, DELETED]
        node: {
          users_some: {
            id: $userId
          }
        }
      }
    ) {
      mutation
      node {
        id
        name
        movies {
          title
          movieId
        }
        users {
          id
          email
        }
      }
      previousValues {
        id
      }
    }
  }
`

export const SUB_FOR_NEW_MOVIES = gql`
  subscription addedMovie($movieListId: ID!) {
    Movie(
      filter: {
        mutation_in: [CREATED, UPDATED, DELETED]
        node: {
          movieList: {
            id: $movieListId
          }
        }
      }
    ) {
      mutation
      node {
        id
        title
        movieId
      }
      previousValues {
        id
      }
    }
  }
`

export const GET_USER_FROM_MAIL = gql`
  query ($email: String!){
    User(email: $email) {
      id
    }
  }
`

export const GET_USER = gql`
  query ($userId: ID!){
    User(id: $userId) {
      id
      email
      movieLists {
        id
        name
        movies {
          title
          movieId
        }
        users {
          email
        }
      }
    }
  }
`

export const GET_MOVIE_LIST = gql`
  query GetMovieList ($movieId: ID!) {
    allMovies(
      filter: {
        movieList: {
          id: $movieId
        }
      }
      orderBy: createdAt_DESC) {
      id
      movieId
      title
    }
  }
`

export const GET_MOVIE_LIST_NAME = gql`
  query ($movieId: ID!){
    MovieList(id: $movieId) {
      name
    }
  }
`
