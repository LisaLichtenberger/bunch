import { createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Login from '@/components/Login'

const localVue = createLocalVue()
localVue.component('Login', Login)
localVue.use(VueRouter)

// const routes = [
//   {
//     path: '/login',
//     name: 'Login',
//     component: Login
//   }
// ]
// const router = new VueRouter({
//   routes
// })

describe('Login', () => {
  it('renders signin header', () => {
    const data = Login.data()
    expect(data.isLogin).to.equal(true)
    expect(data.name).to.equal('')
    data.isLogin = false
    expect(data.isLogin).to.equal(false)
    // var wrapper = factory(data)
    // expect(wrapper.find('.form-signin-heading').text()).equal('Sign Up')
  })
})

// const factory = (values = {}) => {
//   return shallowMount(Login, {
//     localVue,
//     router,
//     name: 'Login',
//     components: {},
//     data () {
//       return {
//         ...values
//       }
//     }
//   })
// }
