import Vue from 'vue'
import SearchBar from '@/components/SearchBar'
// import { mount } from '@vue/test-utils'

describe('SearchBar', () => {
  // it('renders a div with class active when dependency returns true', () => {
  //   const wrapper = mount(SearchBar)
  //   // expect(wrapper.attributes().keypress).to.equal(true)
  //   SearchBar.__Rewire__('dependency', () => true)
  //   wrapper.trigger('keypress.enter')
  //   expect(wrapper.emitted().SearchRequested).to.equal(true)
  //   // expect(wrapper.vm.debounceInput).to.equal(true)
  // })

  // it('renders a div without class active when dependency returns false', () => {
  //   SearchBar.__Rewire__('dependency', () => false)
  //   const wrapper = mount(SearchBar)
  //   expect(wrapper.classes().debounce).to.equal(false)
  //   SearchBar.__ResetDependency__('dependency')
  // })

  it('renders search bar', () => {
    var comp = createComponent()
    expect(comp.query).to.contain('')
  })

  it('searchbar query is updated', (done) => {
    const SearchBar = createComponent()
    var input = SearchBar.$el.querySelector('input')
    input.value = 'universe'
    input.dispatchEvent(new Event('input'))
    Vue.nextTick(() => {
      expect(SearchBar.query).to.contain('universe')
      done() // Call `done` to say we're done
    })
  })
})

function createComponent () {
  const Constructor = Vue.extend(SearchBar)
  return new Constructor().$mount()
}
