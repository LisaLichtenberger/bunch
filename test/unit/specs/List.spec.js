// import Vue from 'vue'
import List from '@/components/List'
import { mount } from '@vue/test-utils'
// import sinon from 'sinon'

describe('List', () => {
  it('click on list item triggers emit message', () => {
    // const clickHandler = sinon.stub()
    const wrapper = mount(List, {
      propsData: {
        list: {
          id: 'cjidckcv70xne0152b5gfjy3z',
          name: 'WG',
          movies: [],
          users: []
        }
      }
    })
    wrapper.find('.list-description').trigger('click')
    expect(wrapper.emitted('selectList').length).to.equal(1)
  })

  it('click on member div triggers emit message', () => {
    // const clickHandler = sinon.stub()
    const wrapper = mount(List, {
      propsData: {
        list: {
          id: 'cjidckcv70xne0152b5gfjy3z',
          name: 'WG',
          movies: [],
          users: []
        }
      }
    })
    wrapper.find('.member').trigger('click')
    expect(wrapper.emitted('showMembers').length).to.equal(1)
  })

  // it('calls handleSearch method', () => {
  //   sinon.spy(SearchBar.methods, 'debounceInput')
  //   const comp = createComponent()
  //
  //   var input = comp.$el.querySelector('input')
  //   input.value = 'test'
  //   input.dispatchEvent(new Event('keypress.enter')) // trigger event that will call giveZero
  //
  //   expect(SearchBar.methods.debounceInput.called).to.equal(true)
  //   SearchBar.methods.debounceInput.restore()
  // })

  it('can display props', () => {
    const wrapper = mount(List, {
      propsData: {
        list: {
          id: 'cjidckcv70xne0152b5gfjy3z',
          name: 'WG',
          movies: [],
          users: []
        }
      }
    })
    expect(wrapper.find('p').text()).to.equal('WG')
  })
})

// function createComponent () {
//   const Constructor = Vue.extend(List)
//   return new Constructor().$mount()
// }
