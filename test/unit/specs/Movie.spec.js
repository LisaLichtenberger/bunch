import Vue from 'vue'
import Movie from '@/components/Movie'
describe('Movie', () => {
  it('movie prop can be set', () => {
    const movie = {
      movieId: 'tt0499549', // imdbID --> not required if custom movie
      Title: 'Avatar',
      Director: 'James Cameron',
      Runtime: '162 min',
      imdbRating: '7.8',
      Poster: 'https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg'
    }
    const movieComp = mount(Movie, { propsData: { movie: movie } })
    expect(movieComp.movie.Title).to.equal('Avatar')
  })
})

function mount (component, options) {
  const Constructor = Vue.extend(component)
  return new Constructor(options).$mount()
}
