// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'nightwatch configuration test': function (browser) {
    browser
      .url('http://www.google.com')
      .waitForElementVisible('body', 1000)
      .setValue('input[type=text]', 'nightwatch')
  },

  'omdb API returns response': function (client) {
    client
      .url('http://www.omdbapi.com/?apikey=aa56b9b3&page=1&s=Avatar')
      .waitForElementVisible('pre', 1000)
      .assert.containsText('pre', '' +
      '{"Search":[{"Title":"Avatar","Year":"2009","imdbID":"tt0499549"')
      .end()
  },

  'login path shows login screen': function (client) {
    client
      .url('https://bunch.blobster.it/#/login')
      .waitForElementVisible('.form-signin-heading', 1000)
      .assert.containsText('.form-signin-heading', 'Log In')
      .setValue('#inputEmail', 'lisa.lichtenberger@gmail.com')
      .keys(client.Keys.ENTER)
      .assert.containsText('.form-signin-heading', 'Log In')
      .end()
  },

  'click on signup link forwards to signup page': function (client) {
    client
      .url('https://bunch.blobster.it/#/login')
      .waitForElementVisible('.form-signin-heading', 1000)
      .assert.containsText('.form-signin-heading', 'Log In')
      .click('a')
      .waitForElementVisible('.form-signin-heading', 1000)
      .assert.containsText('.form-signin-heading', 'Sign Up')
      .end()
  }
}

// module.exports = {
//   'Does not add empty or blank tasks' (client) {
//     client
//       .url('http://todomvc.com/examples/react/#/')
//       .waitForElementVisible('.header h1', 5000)
//       .setValue('.new-todo', 'New task')
//       .keys(client.Keys.ENTER)
//       .keys(client.Keys.ENTER)
//       .setValue('.new-todo', '  ')
//       .keys(client.Keys.ENTER)
//       .assert.containsText('.todo-count', '1 item left')
//       .end()
//   },
//   'omdb API returns response' (client) {
//     client
//       .url('http://www.omdbapi.com/?apikey=aa56b9b3&page=1&s=Avatar')
//       .waitForElementVisible('.header h1', 5000)
//       .setValue('.new-todo', 'New task')
//       .keys(client.Keys.ENTER)
//       .keys(client.Keys.ENTER)
//       .setValue('.new-todo', '  ')
//       .keys(client.Keys.ENTER)
//       .assert.containsText('.todo-count', '1 item left')
//       .end()
//   }
// }
