# bunch

Welcome to Bunch! The end of endless scrolling on Netflix and searching for movies! Connect with your friends and collect ideas for movies you all want to watch and start right off!
Search for movies, add them to your list and add your friends to your list so they can add and share their movies with you as well.

For this project, we got a similar idea as Bring! and therefore we tried to reconstruct it.

Project Requirements:

* Vue.js

* GraphQL server

* test coverage: Unit Tests with Karma and e2e Testing with Nightwatch

* Progressive Web App features: Manifest, Service Workers, Add to Home Screen

* Teams of 2: Lukas Ameisbichler & Lisa Lichtenberger

* Furthermore, we are using the Omdb API to fetch movies with a certain search query the user can enter in a search bar.

Link to our application, which is deployed on AWS:
https://bunch.blobster.it

Link to our repository on Bitbucket:
https://LisaLichtenberger@bitbucket.org/LisaLichtenberger/bunch.git

Link to our video, which shows a screencast of the app�s UI flow:
https://www.dropbox.com/s/uyov82v710cwo3l/BunchDemo.mov?dl=0

In the video, you can see the main flow of our app. By clicking on the hamburger menu, you can see your lists and you can add a new list as well. 
By clicking on the members button, you can view the members of the list add also add new members (by their email addresses) or delete your currently selected list.
On the main screen, you can search for films out of the OMDB database or add custom films as well if they are not available in the database. By clicking
on a movie in the search section, you can add the movie to your list. By clicking on the movie in your list, you can remove it again.

Installation for Bunch: 

* npm install
* npm run start
* Browser: localhost:8080
* run unit tests: npm run unit
* run e2e tests: npm run e2e

Some screenshots:

![alt text](screenshots/addtohome.jpg "Home Screen" | width=48)
![alt text](screenshots/login.png "Home Screen" | width=48)
![alt text](screenshots/signup.png "Home Screen" | width=48)
![alt text](screenshots/home.png "Home Screen" | width=48)
![alt text](screenshots/search.png "Home Screen" | width=48)
![alt text](screenshots/lists.png "Home Screen" | width=48)
![alt text](screenshots/members.png "Home Screen" | width=48)
![alt text](screenshots/wgwatchers.png "Home Screen" | width=48)
